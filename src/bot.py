from bs4 import BeautifulSoup
import urllib3
from sys import exit
from time import sleep
import requests
from xlwt import Workbook


class Response:
    def __init__(self):
        self.http = None

    def _get_response(self,url=None):
        #self.configureUrllib3()
        if url == None:
            print("Please Enter Url")
            exit(1)

        response = requests.get(url)
        return response
    def get_html(self,url):
        response = self._get_response(url)
        self.response = response
        return response.content

class WriteExcel:
    def __init__(self,filename):
        self.filename = filename
        self.workbook = Workbook()
        self.worksheet = self.workbook.add_sheet("Sheet 1")


    def add(self,row=None,col=None,data=None):
            if row == None or col == None:
                print("Row And Col Num cannot be None")
                exit(1)
            self.worksheet.write(row,col,data)
            self.save()
    def save(self):
        self.workbook.save(self.filename)

class CheckConnection:
    def __init__(self):
        self.connection = None

#initialize class
arabamCom = Response()
writeExcel = WriteExcel("arabam.com.xls")


row = 1

unique_users = []
#sehir
#arac tipleri

cities = [
 'adana',
 'adiyaman',
 'afyon',
 'agri',
 'amasya',
 'ankara',
 'antalya',
 'artvin',
 'aydin',
 'balikesir',
 'bilecik',
 'bingol',
 'bitlis',
 'bolu',
 'burdur',
 'bursa',
 'canakkale',
 'cankiri',
 'corum',
 'denizli',
 'diyarbakir',
 'edirne',
 'elazig',
 'erzincan',
 'erzurum',
 'eskisehir',
 'gaziantep',
 'giresun',
 'gumushane',
 'hakkari',
 'hatay',
 'isparta',
 'mersin',
 'istanbul',
 'izmir',
 'kars',
 'kastamonu',
 'kayseri',
 'kirklareli',
 'kirsehir',
 'kocaeli',
 'konya',
 'kutahya',
 'malatya',
 'manisa',
 'kahramanmaras',
 'mardin',
 'mugla',
 'mus',
 'nevsehir',
 'nigde',
 'ordu',
 'rize',
 'sakarya',
 'samsun',
 'siirt',
 'sinop',
 'sivas',
 'tekirdag',
 'tokat',
 'trabzon',
 'tunceli',
 'sanliurfa',
 'usak',
 'van',
 'yozgat',
 'zonguldak',
 'aksaray',
 'bayburt',
 'karaman',
 'kirikkale',
 'batman',
 'sirnak',
 'bartin',
 'ardahan',
 'igdir',
 'yalova',
 'karabuk',
 'kilis',
 'osmaniye',
 'duzce']


types = ['/otomobil','/arazi-suv-pick-up','/motosiklet','/minivan-van_panelvan','/ticari-arac',
            '/kiralik-araclar','/hasarli-araclar','/traktor','/tarim-is-makineleri',
            '/klasik-araclar','/elektrikli-araclar','/atv-utv',
            '/karavan','/engelli-araci','/modifiyeli-araclar']
for city in cities:
    for type in types:
        main_url = "http://www.arabam.com"
        pageNum=1
        while pageNum <= 50:
            try:
                print("Scrapping Page:{}".format(pageNum))
                url = "{}/ikinci-el{}?page={}".format(main_url,type+'-'+city,pageNum)
                html = arabamCom.get_html(url)
                #parse html
                parsed_html = BeautifulSoup(html,'html.parser')
                print("Collecting Links")
                for link in parsed_html.find_all('a'):
                    class_data = link.get('class')
                    if class_data != None and len(class_data) != 0  and class_data[0] == 'listing-title':
                        adv_link = link.get("href")
                        s_html = arabamCom.get_html(main_url+adv_link)
                        print("Collecting Phone Number from link:\n{}".format(adv_link))
                        parsed_s_html = BeautifulSoup(s_html,'html.parser')
                        authorized_person_name = parsed_s_html.find_all('p',{'class':'detail-authorized-person'})
                        if len(authorized_person_name)  == 0:
                            authorized_person_name = parsed_s_html.find_all('p',{'class':'font-default-plus font-letter-shrink bold'})

                        if len(authorized_person_name) == 0:
                            authorized_person_name = parsed_s_html.find_all('p',{'class' : 'color-black2018 font-default-minus bold w100 cb'})


                        if authorized_person_name != None:
                            print(authorized_person_name[0].text)
                            #checking user
                            already_in = False
                            for user in unique_users:
                                if authorized_person_name == user:
                                    already_in = True
                                    break
                            if already_in == False:
                                unique_users.append(authorized_person_name)
                                writeExcel.add(row,1,authorized_person_name[0].text)
                                col = 2 # ascii 'B'
                                for a in parsed_s_html.find_all('a'):
                                    if a.get('href') != None and  a.get('href').split(":")[0] == "tel":
                                        print(a.get('href'))
                                        writeExcel.add(row,col,a.get('href').split(':')[1])
                                        col +=1
                            else:
                                 row = len(unique_users)
                        row+=1
                pageNum+=1

            except requests.ConnectionError:
                wait = input("wait")
                print("Scrapping Page:{}".format(pageNum))
                url = "{}/ikinci-el{}?page={}".format(main_url,type+'-'+city,pageNum)
                html = arabamCom.get_html(url)
                #parse html
                parsed_html = BeautifulSoup(html,'html.parser')
                print("Collecting Links")
                for link in parsed_html.find_all('a'):
                    class_data = link.get('class')
                    if class_data != None and len(class_data) != 0  and class_data[0] == 'listing-title':
                        adv_link = link.get("href")
                        s_html = arabamCom.get_html(main_url+adv_link)
                        print("Collecting Phone Number from link:\n{}".format(adv_link))
                        parsed_s_html = BeautifulSoup(s_html,'html.parser')
                        authorized_person_name = parsed_s_html.find_all('p',{'class':'detail-authorized-person'})
                        if len(authorized_person_name)  == 0:
                            authorized_person_name = parsed_s_html.find_all('p',{'class':'font-default-plus font-letter-shrink bold'})

                        if len(authorized_person_name) == 0:
                            authorized_person_name = parsed_s_html.find_all('p',{'class' : 'color-black2018 font-default-minus bold w100 cb'})


                        if authorized_person_name != None:
                            print(authorized_person_name[0].text)
                            #checking user
                            already_in = False
                            for user in unique_users:
                                if authorized_person_name == user:
                                    already_in = True
                                    break
                            if already_in == False:
                                unique_users.append(authorized_person_name)
                                writeExcel.add(row,1,authorized_person_name[0].text)
                                col = 2 # ascii 'B'
                                for a in parsed_s_html.find_all('a'):
                                    if a.get('href') != None and  a.get('href').split(":")[0] == "tel":
                                        print(a.get('href'))
                                        writeExcel.add(row,col,a.get('href').split(':')[1])
                                        col +=1
                            else:
                                 row = len(unique_users)
                        row+=1
                pageNum+=1
